import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InicioComponent } from './components/inicio/inicio.component';
import { DatosGeneralesComponent } from './components/datos-generales/datos-generales.component';
import { ResumenComponent } from './components/resumen/resumen.component';
import { InformacionDeContactoComponent } from './components/informacion-de-contacto/informacion-de-contacto.component';
import { AgendaElectronicaComponent } from './components/agenda-electronica/agenda-electronica.component';


const routes: Routes = [
  {path: 'inicio', component: InicioComponent},
  {path: 'datos-generales', component: DatosGeneralesComponent},
  {path: 'resumen', component: ResumenComponent},
  {path: 'informacion-de-contacto', component: InformacionDeContactoComponent},
  {path: 'agenda-electronica', component: AgendaElectronicaComponent},
  {path: '**', pathMatch: 'full', redirectTo: 'inicio'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
